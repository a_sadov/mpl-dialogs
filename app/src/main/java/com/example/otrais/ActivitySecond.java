package com.example.otrais;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class ActivitySecond extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
       Button act_1 = findViewById(R.id.buttonA1);
       act_1.setOnClickListener(new View.OnClickListener(){
          @Override
           public void onClick(View v){
             // finish(); //sledz sho aktivitaati un atgriezhas
              // Intent intent = new Intent( ActivitySecond.this, MainActivity.class);
                //startActivity(intent);
              Intent intent = new Intent( ActivitySecond.this, MainActivity.class);
              intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); //notiira ieprieksheejo veestury
              startActivity(intent);
            }
        });
    }
}
