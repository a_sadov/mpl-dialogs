package com.example.otrais;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.Arrays;
import java.util.List;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button act_2 = findViewById(R.id.buttonAC);
        act_2.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent intent = new Intent( MainActivity.this, ActivitySecond.class);
                startActivity(intent);
            }
        });

        Button dia = findViewById(R.id.buttonD);
        dia.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Toast.makeText(MainActivity.this, "text", Toast.LENGTH_LONG).show();
                AlertDialog.Builder builder=new AlertDialog.Builder(MainActivity.this);
               //builder.setMessage(getString(R.string.dia_title))
                     builder.setTitle(getString(R.string.dia_title));

                builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User clicked OK button
                        Toast.makeText(MainActivity.this, "OK", Toast.LENGTH_LONG).show();
                    }
                });
                builder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                        Toast.makeText(MainActivity.this, "cancelled", Toast.LENGTH_LONG).show();
                    }
                });
                final List<String> students = Arrays.asList(getResources().getStringArray(R.array.students));
                builder.setMultiChoiceItems(R.array.students, null,
                        new DialogInterface.OnMultiChoiceClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which,
                                                boolean isChecked) {
                                if (isChecked) {
                                    Toast.makeText(MainActivity.this, "Izvēlēts " + students.get(which), Toast.LENGTH_LONG).show();

                                } else {

                                    Toast.makeText(MainActivity.this, "Neizvēlēts " + students.get(which), Toast.LENGTH_LONG).show();
                                }
                            }
                        });
                        AlertDialog dialog = builder.create();
                        dialog.show();
           //     AlertDialog.Builder builder = new AlertDIalog.Builder (MainActivity:this);

            }

            }

        );

    }
}
